﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace ArqInf.Models
{
    public class User : IdentityUser
    {
        public int EmployeeNumber { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        public char Gender  { get; set; }

        public Role Role { get; set; }
    }
}
