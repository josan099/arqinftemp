﻿namespace ArqInf.Models
{
    public class Role
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public string Department { get; set; }

    }
}
